<?php

require_once('DBConnector.php'); //including DBConnector class file

//initializing the DBConnector object
$dbobj = DBConnector::initWithValue('localhost','root','aulajaula','dbtest');


/* Demo insert Query */
$sql = "INSERT INTO `users` (`name`, `email`) VALUES ('demo','demo@gmail.com');";
$dbobj->insertData($sql);


/* //Demo Delete Query

$sql = "DELETE FROM `users` WHERE id = 2 ;";
$dbobj->deleteData($sql);
*/

/* //Demo Update query

$sql = "UPDATE users SET name = 'newuser' WHERE id = 1;";
$dbobj->updateData($sql);
*/

/* //Demo Select Query

$sql = "SELECT * from users;";
$result = $dbobj->selectData($sql);

while($row = $result->fetch_assoc())
{
    echo "id: " . $row["id"]. " - Name: " . $row["name"]. " - Email: " . $row["email"]. "\n";
}
*/

?>