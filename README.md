###Sample overview of the files

- **DBConnector.php** => This class is responsible for Connecting Database and perform Query on that DB.
- **DBtest.php** => Just only to test the DBConnector class. Also consists the functionality how to use the DBConnector class.
- **XMLParser.php** => XMLParser class to read write xml data from a xml file.
- **config.xml** => A demo xml file.
- **Configform.php** => A php file with a html form to change in xml file. Giving input in the form fields and then hitting submit will take the data from input fields and then uses XMLParser class to save these data to config.xml file.
- **insertform.php**=> A php file with html form to show demo how to really perform query in DB. Giving input in the form fields and then hitting submit will take the data from input fields and then uses XMLParser and DBConnector to insert this data to DB. This is actually a real life example of how to use those Classes.
- **XMLtest.php** => A sample code to test the XMLParser class, just for testing purpose.