<?php

//error_reporting(E_ALL);
require_once('XMLparser.php');
require_once('DBConnector.php');

$newobj = new XMLparser('config.xml');
$newobj->parse();

//initializing DB object with the config data specified in XML file
$dbobj = DBConnector::initWithValue($newobj->getValue('dbhost'),$newobj->getValue('dbuser'),$newobj->getValue('dbpass'),$newobj->getValue('dbname'));

if(isset($_POST['deleteform']))
{
	$userid = $_POST['userid'];
	$sql = "DELETE FROM `users` WHERE id = $userid ;";
	$dbobj->deleteData($sql);
}

$sql = "SELECT * from users;";
$result = $dbobj->selectData($sql);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<style>
		table, th {
		    border: 1px solid #A7C942;
		    margin: 0px;
		    padding: 0px;
		    font-size: 16px;
		}

		th {
		    background-color: #A7C942;
		    color: white;
		}
	</style>
</head>
<body>
	<h3>Users table Data:</h3>
	<table>
		<thead>
			<tr>
				<th>User Name</th>
				<th>User Email</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				while($row = $result->fetch_assoc())
				{

					echo "<tr>";

					echo "<td>".$row['name']."</td>";
					echo "<td>".$row['email']."</td>";
					echo "<td>";
					echo "<form action='deleteform.php' method='post' >";
					echo "<input name='userid' type='hidden' value='".$row['id']."'>";
					echo "<input name='deleteform' type='submit' value='delete'/>";
					echo "</form>";
					echo "</td>";

					echo "</tr>";

				}
			?>
		</tbody>
	</table>

</body>
</html>