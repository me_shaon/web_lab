<?php

require_once('XMLparser.php'); //including the XMLParser class

$newobj = new XMLparser('config.xml');	//Creating XMLParser object
$newobj->parse(); //parse data from xml file

if(isset($_POST['submitform'])) //when a form is submitted to this page, this condition will be true
{
	//Setting newly submitted data to xmlparser object
	$newobj->setValue('title',  $_POST['title']);
	$newobj->setValue('dbuser', $_POST['dbuser']);
	$newobj->setValue('dbname', $_POST['dbname']);
	$newobj->setValue('dbhost', $_POST['dbhost']);
	$newobj->setValue('dbpass', $_POST['dbpass']);

	$newobj->saveXML(); //saving data to xml file
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>XML Data</title>
</head>
<body>
	<h1>Configuration Settings</h1>
	<form action="configform.php" method="post">
		<label for="">Title: </label>
		<input type="text" name="title" value="<?= $newobj->getValue('title') ?>"><br/>

		<label for="">DB Host:</label>
		<input type="text" name="dbhost" value="<?= $newobj->getValue('dbhost') ?>"><br/>

		<label for="">DB Name:</label>
		<input type="text" name="dbname" value="<?= $newobj->getValue('dbname') ?>"><br/>

		<label for="">DB User Name:</label>
		<input type="text" name="dbuser" value="<?= $newobj->getValue('dbuser') ?>"><br/>

		<label for="">DB Pass:</label>
		<input type="password" name="dbpass" value=""><br/>

		<input type="submit" name="submitform" value="Save">
	</form>

</body>
</html>
