<?php
	/***
	** 	XML parser class for Read and Write XML data
	***/
	class XMLparser
	{
		private $xml;
		private $json;
		private $filename;
		private $data;

		/**
		*	Initializing the constructor with a filename consisting xml data
		*	This constructor takes a string of filename as a parameter
		**/
		public function __construct($filename)
		{
			$this->filename = $filename;
		}

		/**
		*	This function parse xml data from the file and first encode it
		*	to json string and then decode it to an object
		**/
		public function parse()
		{
			$this->xml  = simplexml_load_file($this->filename); //load the xml file with SimpleXML Module
			$this->json = json_encode($this->xml);	//Convert the xml data to JSON String format
			$this->data = json_decode($this->json,true); //Convert the JSON String to JSON object
			//var_dump($this->data);
		}

		/**
		*	This funciton returns the value associated with a specific key
		*	of the data object
		**/
		public function getValue($key)
		{
			if($this->data)
				return $this->data[$key];
			return "Key not found";
		}

		/**
		*	This function Sets a value to a specific key on the data object
		**/
		public function setValue($key, $value)
		{
			if($this->data)
				$this->data[$key] = $value;
			return "Object not found";
		}

		/**
		*	After changes to the data object,
		*	This function saves changed JSON Object to xml file,
		*	and load the newly changed xml data to the data object of this class
		**/
		public function saveXML()
		{
			$this->xml = new SimpleXMLElement('<appconfig/>');

			$this->data = array_flip($this->data);

			array_walk_recursive($this->data, array($this->xml, 'addChild'));

			$this->xml->asXML($this->filename);

			$this->parse();
		}

		/**
		*	This function recursively check for the data in $this->data object based on the key speicified in search string and return the entire array specified with that key
		*	$list, is the array with the token given in search string, like $list[0]='appconfig',$list[1]='user' from the string '/appconfig/user'
		*	$index, is the current index to search in $list array
		*	$searchfield, is the currently searching array in $this->data object
		*	$listsize, is the total number of element in $list array
		**/

		public function find_recursive_data($list,$index,$searchfield,$listsize)
		{
			$key = $list[$index];

			//echo "Current: \n";
			//print_r($searchfield);



			if($this->xml->getName() == $key) //for the root node, here 'appconfig'
			{
				if($index == $listsize-1)
					return $searchfield;
				return $this->find_recursive_data($list,$index+1,$searchfield,$listsize);
			}
			else if(array_key_exists($key, $searchfield)) //Searching key exists in array
			{
				if($index == $listsize-1)
				{
					return $searchfield[$key];
				}
				else
					return $this->find_recursive_data($list,$index+1,$searchfield[$key],$listsize);
			}
			else if(is_array($searchfield)) //if exact key not found and searchfield is an array
			{
				$formattedfield = array($key => array());
				$found = false;
				foreach ($searchfield as $singledata)
				{
					if(array_key_exists($key, $singledata))
					{
						array_push($formattedfield[$key],$singledata[$key]);
						$found = true;
					}
				}
				if(!$found)
					return 'not found';

				return $this->find_recursive_data($list,$index,$formattedfield,$listsize);
			}
			else
				return 'not found';
		}

		/**
		*	This function takes the string, search path, to search in xml data object, like '/appconfig/user'
		**/
		public function xpathsearch($path)
		{
			//print_r($this->data);

			$tokpath = explode('/', $path); //tokenizing the given path

			$searchlength = count($tokpath);

			if($searchlength>=1)
				$result = $this->find_recursive_data($tokpath,1,$this->data,$searchlength);

			print_r($result);

			echo "\n";
		}


	}


?>
