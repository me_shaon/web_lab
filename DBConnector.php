<?php

class DBConnector
{

	private $servername;
	private $username;
	private $password;
	private $dbname;
	private $conn;


	public function __cosntruct()
	{
		$this->servername 	= 'localhost';
		$this->username 	= 'root';
		$this->password 	= '';
		$this->dbname 		= 'test';
	}

	public static function initWithValue($server, $user, $pass, $db)
	{
		$newobj = new self();

		$newobj->servername = $server;
		$newobj->username 	= $user;
		$newobj->password 	= $pass;
		$newobj->dbname 	= $db;

		return $newobj;
	}


	private function executeQuery($sql)
	{

		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		$result = null;

		if ($conn->connect_error)
		{
		    die("Connection failed: " . $conn->connect_error);
		}

		$result = $conn->query($sql);

		$conn->close();

		return $result;
	}

	public function insertData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				echo "Data inserted successfully";
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else
		{
			die('Wrong Insert Query: $sql');
		}
	}

	public function deleteData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				echo "Data deleted successfully";
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else
		{
			die('Wrong Delete Query: $sql');
		}
	}

	public function selectData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);

			return $result;
		}
		else
		{
			die('Wrong Select Query: $sql');
		}
	}

	public function updateData($sql)
	{
		if($sql)
		{
			$result = $this->executeQuery($sql);
			if ($result)
			{
				echo "Data updated successfully";
			}
			else
			{
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else
		{
			die('Wrong Update Query: $sql');
		}
	}


}


?>
