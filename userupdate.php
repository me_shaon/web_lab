<?php
/* Sample page for inserting data in DB using XMLParser and DBConnector Class */

require_once('XMLparser.php');
require_once('DBConnector.php');

$newobj = new XMLparser('config.xml');
$newobj->parse();

//initializing DB object with the config data specified in XML file
$dbobj = DBConnector::initWithValue($newobj->getValue('dbhost'),$newobj->getValue('dbuser'),$newobj->getValue('dbpass'),$newobj->getValue('dbname'));

if(isset($_POST['submitform'])) //'submitform' is the name of <form> in html
{
	$name 	= $_POST['name'];
	$email 	= $_POST['email'];
	$userid = $_POST['userid'];

	$sql = "UPDATE users SET name = '$name', email = '$email' WHERE id = $userid;";
	$dbobj->updateData($sql);


	$url = "./updateform.php";
	header("Location: ".$url); //redirecting to another php file
	die();

}

$userid = $_GET['userid'];

$sql = "SELECT * from users where id = $userid;";
$result = $dbobj->selectData($sql);

while($row = $result->fetch_assoc())
{
	$userid = $row['id'];
	$username = $row['name'];
	$useremail = $row['email'];
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1>User Info Update:</h1>
	<form action="" method="post">
		<label for="">Name: </label>
		<input type="text" name="name" value="<?=$username?>"><br/>

		<label for="">Email:</label>
		<input type="text" name="email" value="<?=$useremail?>"><br/>

		<input type="hidden" name="userid" value="<?=$userid?>">
		<input type="submit" name="submitform" value="Save">
	</form>

</body>
</html>