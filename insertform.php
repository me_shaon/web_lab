<?php
/* Sample page for inserting data in DB using XMLParser and DBConnector Class */

require_once('XMLparser.php');
require_once('DBConnector.php');

$newobj = new XMLparser('config.xml');
$newobj->parse();

//initializing DB object with the config data specified in XML file
$dbobj = DBConnector::initWithValue($newobj->getValue('dbhost'),$newobj->getValue('dbuser'),$newobj->getValue('dbpass'),$newobj->getValue('dbname'));


if(isset($_POST['submitform'])) //'submitform' is the name of <form> in html
{
	$name = $_POST['name'];
	$email = $_POST['email'];

	//following sql is for my database table, you may need to change the query for yours
	$sql = "INSERT INTO `users` (`name`, `email`) VALUES ('$name','$email');";
	$dbobj->insertData($sql);
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1>New User Insert:</h1>
	<form action="" method="post">
		<label for="">Name: </label>
		<input type="text" name="name" value=""><br/>

		<label for="">Email:</label>
		<input type="text" name="email" value=""><br/>


		<input type="submit" name="submitform" value="Save">
	</form>

</body>
</html>